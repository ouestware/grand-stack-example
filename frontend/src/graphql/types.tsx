import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type _AddPersonActed_InPayload = {
  __typename?: '_AddPersonActed_inPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _RemovePersonActed_InPayload = {
  __typename?: '_RemovePersonActed_inPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _MergePersonActed_InPayload = {
  __typename?: '_MergePersonActed_inPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _PersonActed_In_Rel = {
  __typename?: '_PersonACTED_IN_rel';
  roles: Array<Maybe<Scalars['String']>>;
  _id: Maybe<Scalars['String']>;
  Movie: Maybe<Movie>;
};

export type _PersonActed_InFilter = {
  AND: Maybe<Array<_PersonActed_InFilter>>;
  OR: Maybe<Array<_PersonActed_InFilter>>;
  Movie: Maybe<_MovieFilter>;
};

export enum _Acted_InOrdering {
  IdAsc = '_id_asc',
  IdDesc = '_id_desc'
}

export type _Acted_InInput = {
  roles: Array<Maybe<Scalars['String']>>;
};

export type _AddPersonActed_In_RelPayload = {
  __typename?: '_AddPersonACTED_IN_relPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
  roles: Array<Maybe<Scalars['String']>>;
  _id: Maybe<Scalars['String']>;
};

export type _RemovePersonActed_In_RelPayload = {
  __typename?: '_RemovePersonACTED_IN_relPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _UpdatePersonActed_In_RelPayload = {
  __typename?: '_UpdatePersonACTED_IN_relPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
  roles: Array<Maybe<Scalars['String']>>;
  _id: Maybe<Scalars['String']>;
};

export type _MergePersonActed_In_RelPayload = {
  __typename?: '_MergePersonACTED_IN_relPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
  roles: Array<Maybe<Scalars['String']>>;
  _id: Maybe<Scalars['String']>;
};

export type _AddPersonDirectedPayload = {
  __typename?: '_AddPersonDirectedPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _RemovePersonDirectedPayload = {
  __typename?: '_RemovePersonDirectedPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _MergePersonDirectedPayload = {
  __typename?: '_MergePersonDirectedPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _AddPersonProducedPayload = {
  __typename?: '_AddPersonProducedPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _RemovePersonProducedPayload = {
  __typename?: '_RemovePersonProducedPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _MergePersonProducedPayload = {
  __typename?: '_MergePersonProducedPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _AddPersonWrotePayload = {
  __typename?: '_AddPersonWrotePayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _RemovePersonWrotePayload = {
  __typename?: '_RemovePersonWrotePayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _MergePersonWrotePayload = {
  __typename?: '_MergePersonWrotePayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _AddPersonFollowsPayload = {
  __typename?: '_AddPersonFollowsPayload';
  from: Maybe<Person>;
  to: Maybe<Person>;
};

export type _RemovePersonFollowsPayload = {
  __typename?: '_RemovePersonFollowsPayload';
  from: Maybe<Person>;
  to: Maybe<Person>;
};

export type _MergePersonFollowsPayload = {
  __typename?: '_MergePersonFollowsPayload';
  from: Maybe<Person>;
  to: Maybe<Person>;
};

export type _AddPersonReviewedPayload = {
  __typename?: '_AddPersonReviewedPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _RemovePersonReviewedPayload = {
  __typename?: '_RemovePersonReviewedPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _MergePersonReviewedPayload = {
  __typename?: '_MergePersonReviewedPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _PersonReviewed_Rel = {
  __typename?: '_PersonREVIEWED_rel';
  rating: Scalars['Int'];
  summary: Scalars['String'];
  _id: Maybe<Scalars['String']>;
  Movie: Maybe<Movie>;
};

export type _PersonReviewedFilter = {
  AND: Maybe<Array<_PersonReviewedFilter>>;
  OR: Maybe<Array<_PersonReviewedFilter>>;
  rating: Maybe<Scalars['Int']>;
  rating_not: Maybe<Scalars['Int']>;
  rating_in: Maybe<Array<Scalars['Int']>>;
  rating_not_in: Maybe<Array<Scalars['Int']>>;
  rating_lt: Maybe<Scalars['Int']>;
  rating_lte: Maybe<Scalars['Int']>;
  rating_gt: Maybe<Scalars['Int']>;
  rating_gte: Maybe<Scalars['Int']>;
  summary: Maybe<Scalars['String']>;
  summary_not: Maybe<Scalars['String']>;
  summary_in: Maybe<Array<Scalars['String']>>;
  summary_not_in: Maybe<Array<Scalars['String']>>;
  summary_contains: Maybe<Scalars['String']>;
  summary_not_contains: Maybe<Scalars['String']>;
  summary_starts_with: Maybe<Scalars['String']>;
  summary_not_starts_with: Maybe<Scalars['String']>;
  summary_ends_with: Maybe<Scalars['String']>;
  summary_not_ends_with: Maybe<Scalars['String']>;
  Movie: Maybe<_MovieFilter>;
};

export enum _ReviewedOrdering {
  RatingAsc = 'rating_asc',
  RatingDesc = 'rating_desc',
  SummaryAsc = 'summary_asc',
  SummaryDesc = 'summary_desc',
  IdAsc = '_id_asc',
  IdDesc = '_id_desc'
}

export type _ReviewedInput = {
  rating: Scalars['Int'];
  summary: Scalars['String'];
};

export type _AddPersonReviewed_RelPayload = {
  __typename?: '_AddPersonREVIEWED_relPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
  rating: Scalars['Int'];
  summary: Scalars['String'];
  _id: Maybe<Scalars['String']>;
};

export type _RemovePersonReviewed_RelPayload = {
  __typename?: '_RemovePersonREVIEWED_relPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _UpdatePersonReviewed_RelPayload = {
  __typename?: '_UpdatePersonREVIEWED_relPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
  rating: Scalars['Int'];
  summary: Scalars['String'];
  _id: Maybe<Scalars['String']>;
};

export type _MergePersonReviewed_RelPayload = {
  __typename?: '_MergePersonREVIEWED_relPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
  rating: Scalars['Int'];
  summary: Scalars['String'];
  _id: Maybe<Scalars['String']>;
};

export type _PersonInput = {
  name: Scalars['String'];
};

export enum _PersonOrdering {
  BornAsc = 'born_asc',
  BornDesc = 'born_desc',
  NameAsc = 'name_asc',
  NameDesc = 'name_desc',
  IdAsc = '_id_asc',
  IdDesc = '_id_desc'
}

export type _PersonFilter = {
  AND: Maybe<Array<_PersonFilter>>;
  OR: Maybe<Array<_PersonFilter>>;
  born: Maybe<Scalars['Int']>;
  born_not: Maybe<Scalars['Int']>;
  born_in: Maybe<Array<Scalars['Int']>>;
  born_not_in: Maybe<Array<Scalars['Int']>>;
  born_lt: Maybe<Scalars['Int']>;
  born_lte: Maybe<Scalars['Int']>;
  born_gt: Maybe<Scalars['Int']>;
  born_gte: Maybe<Scalars['Int']>;
  name: Maybe<Scalars['String']>;
  name_not: Maybe<Scalars['String']>;
  name_in: Maybe<Array<Scalars['String']>>;
  name_not_in: Maybe<Array<Scalars['String']>>;
  name_contains: Maybe<Scalars['String']>;
  name_not_contains: Maybe<Scalars['String']>;
  name_starts_with: Maybe<Scalars['String']>;
  name_not_starts_with: Maybe<Scalars['String']>;
  name_ends_with: Maybe<Scalars['String']>;
  name_not_ends_with: Maybe<Scalars['String']>;
  acted_in: Maybe<_MovieFilter>;
  acted_in_not: Maybe<_MovieFilter>;
  acted_in_in: Maybe<Array<_MovieFilter>>;
  acted_in_not_in: Maybe<Array<_MovieFilter>>;
  acted_in_some: Maybe<_MovieFilter>;
  acted_in_none: Maybe<_MovieFilter>;
  acted_in_single: Maybe<_MovieFilter>;
  acted_in_every: Maybe<_MovieFilter>;
  ACTED_IN_rel: Maybe<_PersonActed_InFilter>;
  ACTED_IN_rel_not: Maybe<_PersonActed_InFilter>;
  ACTED_IN_rel_in: Maybe<Array<_PersonActed_InFilter>>;
  ACTED_IN_rel_not_in: Maybe<Array<_PersonActed_InFilter>>;
  ACTED_IN_rel_some: Maybe<_PersonActed_InFilter>;
  ACTED_IN_rel_none: Maybe<_PersonActed_InFilter>;
  ACTED_IN_rel_single: Maybe<_PersonActed_InFilter>;
  ACTED_IN_rel_every: Maybe<_PersonActed_InFilter>;
  directed: Maybe<_MovieFilter>;
  directed_not: Maybe<_MovieFilter>;
  directed_in: Maybe<Array<_MovieFilter>>;
  directed_not_in: Maybe<Array<_MovieFilter>>;
  directed_some: Maybe<_MovieFilter>;
  directed_none: Maybe<_MovieFilter>;
  directed_single: Maybe<_MovieFilter>;
  directed_every: Maybe<_MovieFilter>;
  produced: Maybe<_MovieFilter>;
  produced_not: Maybe<_MovieFilter>;
  produced_in: Maybe<Array<_MovieFilter>>;
  produced_not_in: Maybe<Array<_MovieFilter>>;
  produced_some: Maybe<_MovieFilter>;
  produced_none: Maybe<_MovieFilter>;
  produced_single: Maybe<_MovieFilter>;
  produced_every: Maybe<_MovieFilter>;
  wrote: Maybe<_MovieFilter>;
  wrote_not: Maybe<_MovieFilter>;
  wrote_in: Maybe<Array<_MovieFilter>>;
  wrote_not_in: Maybe<Array<_MovieFilter>>;
  wrote_some: Maybe<_MovieFilter>;
  wrote_none: Maybe<_MovieFilter>;
  wrote_single: Maybe<_MovieFilter>;
  wrote_every: Maybe<_MovieFilter>;
  follows: Maybe<_PersonFilter>;
  follows_not: Maybe<_PersonFilter>;
  follows_in: Maybe<Array<_PersonFilter>>;
  follows_not_in: Maybe<Array<_PersonFilter>>;
  follows_some: Maybe<_PersonFilter>;
  follows_none: Maybe<_PersonFilter>;
  follows_single: Maybe<_PersonFilter>;
  follows_every: Maybe<_PersonFilter>;
  reviewed: Maybe<_MovieFilter>;
  reviewed_not: Maybe<_MovieFilter>;
  reviewed_in: Maybe<Array<_MovieFilter>>;
  reviewed_not_in: Maybe<Array<_MovieFilter>>;
  reviewed_some: Maybe<_MovieFilter>;
  reviewed_none: Maybe<_MovieFilter>;
  reviewed_single: Maybe<_MovieFilter>;
  reviewed_every: Maybe<_MovieFilter>;
  REVIEWED_rel: Maybe<_PersonReviewedFilter>;
  REVIEWED_rel_not: Maybe<_PersonReviewedFilter>;
  REVIEWED_rel_in: Maybe<Array<_PersonReviewedFilter>>;
  REVIEWED_rel_not_in: Maybe<Array<_PersonReviewedFilter>>;
  REVIEWED_rel_some: Maybe<_PersonReviewedFilter>;
  REVIEWED_rel_none: Maybe<_PersonReviewedFilter>;
  REVIEWED_rel_single: Maybe<_PersonReviewedFilter>;
  REVIEWED_rel_every: Maybe<_PersonReviewedFilter>;
};

export type Person = {
  __typename?: 'Person';
  _id: Maybe<Scalars['String']>;
  born: Maybe<Scalars['Int']>;
  name: Scalars['String'];
  acted_in: Maybe<Array<Maybe<Movie>>>;
  ACTED_IN_rel: Maybe<Array<Maybe<_PersonActed_In_Rel>>>;
  directed: Maybe<Array<Maybe<Movie>>>;
  produced: Maybe<Array<Maybe<Movie>>>;
  wrote: Maybe<Array<Maybe<Movie>>>;
  follows: Maybe<Array<Maybe<Person>>>;
  reviewed: Maybe<Array<Maybe<Movie>>>;
  REVIEWED_rel: Maybe<Array<Maybe<_PersonReviewed_Rel>>>;
};


export type PersonActed_InArgs = {
  first: Maybe<Scalars['Int']>;
  offset: Maybe<Scalars['Int']>;
  orderBy: Maybe<Array<Maybe<_MovieOrdering>>>;
  filter: Maybe<_MovieFilter>;
};


export type PersonActed_In_RelArgs = {
  first: Maybe<Scalars['Int']>;
  offset: Maybe<Scalars['Int']>;
  filter: Maybe<_PersonActed_InFilter>;
};


export type PersonDirectedArgs = {
  first: Maybe<Scalars['Int']>;
  offset: Maybe<Scalars['Int']>;
  orderBy: Maybe<Array<Maybe<_MovieOrdering>>>;
  filter: Maybe<_MovieFilter>;
};


export type PersonProducedArgs = {
  first: Maybe<Scalars['Int']>;
  offset: Maybe<Scalars['Int']>;
  orderBy: Maybe<Array<Maybe<_MovieOrdering>>>;
  filter: Maybe<_MovieFilter>;
};


export type PersonWroteArgs = {
  first: Maybe<Scalars['Int']>;
  offset: Maybe<Scalars['Int']>;
  orderBy: Maybe<Array<Maybe<_MovieOrdering>>>;
  filter: Maybe<_MovieFilter>;
};


export type PersonFollowsArgs = {
  first: Maybe<Scalars['Int']>;
  offset: Maybe<Scalars['Int']>;
  orderBy: Maybe<Array<Maybe<_PersonOrdering>>>;
  filter: Maybe<_PersonFilter>;
};


export type PersonReviewedArgs = {
  first: Maybe<Scalars['Int']>;
  offset: Maybe<Scalars['Int']>;
  orderBy: Maybe<Array<Maybe<_MovieOrdering>>>;
  filter: Maybe<_MovieFilter>;
};


export type PersonReviewed_RelArgs = {
  first: Maybe<Scalars['Int']>;
  offset: Maybe<Scalars['Int']>;
  orderBy: Maybe<Array<Maybe<_ReviewedOrdering>>>;
  filter: Maybe<_PersonReviewedFilter>;
};

export type _AddMoviePersons_Acted_InPayload = {
  __typename?: '_AddMoviePersons_acted_inPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _RemoveMoviePersons_Acted_InPayload = {
  __typename?: '_RemoveMoviePersons_acted_inPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _MergeMoviePersons_Acted_InPayload = {
  __typename?: '_MergeMoviePersons_acted_inPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _AddMoviePersons_DirectedPayload = {
  __typename?: '_AddMoviePersons_directedPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _RemoveMoviePersons_DirectedPayload = {
  __typename?: '_RemoveMoviePersons_directedPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _MergeMoviePersons_DirectedPayload = {
  __typename?: '_MergeMoviePersons_directedPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _AddMoviePersons_ProducedPayload = {
  __typename?: '_AddMoviePersons_producedPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _RemoveMoviePersons_ProducedPayload = {
  __typename?: '_RemoveMoviePersons_producedPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _MergeMoviePersons_ProducedPayload = {
  __typename?: '_MergeMoviePersons_producedPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _AddMoviePersons_WrotePayload = {
  __typename?: '_AddMoviePersons_wrotePayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _RemoveMoviePersons_WrotePayload = {
  __typename?: '_RemoveMoviePersons_wrotePayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _MergeMoviePersons_WrotePayload = {
  __typename?: '_MergeMoviePersons_wrotePayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _AddMoviePersons_ReviewedPayload = {
  __typename?: '_AddMoviePersons_reviewedPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _RemoveMoviePersons_ReviewedPayload = {
  __typename?: '_RemoveMoviePersons_reviewedPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _MergeMoviePersons_ReviewedPayload = {
  __typename?: '_MergeMoviePersons_reviewedPayload';
  from: Maybe<Person>;
  to: Maybe<Movie>;
};

export type _MovieInput = {
  title: Scalars['String'];
};

export enum _MovieOrdering {
  ReleasedAsc = 'released_asc',
  ReleasedDesc = 'released_desc',
  TaglineAsc = 'tagline_asc',
  TaglineDesc = 'tagline_desc',
  TitleAsc = 'title_asc',
  TitleDesc = 'title_desc',
  IdAsc = '_id_asc',
  IdDesc = '_id_desc'
}

export type _MovieFilter = {
  AND: Maybe<Array<_MovieFilter>>;
  OR: Maybe<Array<_MovieFilter>>;
  released: Maybe<Scalars['Int']>;
  released_not: Maybe<Scalars['Int']>;
  released_in: Maybe<Array<Scalars['Int']>>;
  released_not_in: Maybe<Array<Scalars['Int']>>;
  released_lt: Maybe<Scalars['Int']>;
  released_lte: Maybe<Scalars['Int']>;
  released_gt: Maybe<Scalars['Int']>;
  released_gte: Maybe<Scalars['Int']>;
  tagline: Maybe<Scalars['String']>;
  tagline_not: Maybe<Scalars['String']>;
  tagline_in: Maybe<Array<Scalars['String']>>;
  tagline_not_in: Maybe<Array<Scalars['String']>>;
  tagline_contains: Maybe<Scalars['String']>;
  tagline_not_contains: Maybe<Scalars['String']>;
  tagline_starts_with: Maybe<Scalars['String']>;
  tagline_not_starts_with: Maybe<Scalars['String']>;
  tagline_ends_with: Maybe<Scalars['String']>;
  tagline_not_ends_with: Maybe<Scalars['String']>;
  title: Maybe<Scalars['String']>;
  title_not: Maybe<Scalars['String']>;
  title_in: Maybe<Array<Scalars['String']>>;
  title_not_in: Maybe<Array<Scalars['String']>>;
  title_contains: Maybe<Scalars['String']>;
  title_not_contains: Maybe<Scalars['String']>;
  title_starts_with: Maybe<Scalars['String']>;
  title_not_starts_with: Maybe<Scalars['String']>;
  title_ends_with: Maybe<Scalars['String']>;
  title_not_ends_with: Maybe<Scalars['String']>;
  persons_acted_in: Maybe<_PersonFilter>;
  persons_acted_in_not: Maybe<_PersonFilter>;
  persons_acted_in_in: Maybe<Array<_PersonFilter>>;
  persons_acted_in_not_in: Maybe<Array<_PersonFilter>>;
  persons_acted_in_some: Maybe<_PersonFilter>;
  persons_acted_in_none: Maybe<_PersonFilter>;
  persons_acted_in_single: Maybe<_PersonFilter>;
  persons_acted_in_every: Maybe<_PersonFilter>;
  persons_directed: Maybe<_PersonFilter>;
  persons_directed_not: Maybe<_PersonFilter>;
  persons_directed_in: Maybe<Array<_PersonFilter>>;
  persons_directed_not_in: Maybe<Array<_PersonFilter>>;
  persons_directed_some: Maybe<_PersonFilter>;
  persons_directed_none: Maybe<_PersonFilter>;
  persons_directed_single: Maybe<_PersonFilter>;
  persons_directed_every: Maybe<_PersonFilter>;
  persons_produced: Maybe<_PersonFilter>;
  persons_produced_not: Maybe<_PersonFilter>;
  persons_produced_in: Maybe<Array<_PersonFilter>>;
  persons_produced_not_in: Maybe<Array<_PersonFilter>>;
  persons_produced_some: Maybe<_PersonFilter>;
  persons_produced_none: Maybe<_PersonFilter>;
  persons_produced_single: Maybe<_PersonFilter>;
  persons_produced_every: Maybe<_PersonFilter>;
  persons_wrote: Maybe<_PersonFilter>;
  persons_wrote_not: Maybe<_PersonFilter>;
  persons_wrote_in: Maybe<Array<_PersonFilter>>;
  persons_wrote_not_in: Maybe<Array<_PersonFilter>>;
  persons_wrote_some: Maybe<_PersonFilter>;
  persons_wrote_none: Maybe<_PersonFilter>;
  persons_wrote_single: Maybe<_PersonFilter>;
  persons_wrote_every: Maybe<_PersonFilter>;
  persons_reviewed: Maybe<_PersonFilter>;
  persons_reviewed_not: Maybe<_PersonFilter>;
  persons_reviewed_in: Maybe<Array<_PersonFilter>>;
  persons_reviewed_not_in: Maybe<Array<_PersonFilter>>;
  persons_reviewed_some: Maybe<_PersonFilter>;
  persons_reviewed_none: Maybe<_PersonFilter>;
  persons_reviewed_single: Maybe<_PersonFilter>;
  persons_reviewed_every: Maybe<_PersonFilter>;
};

export type Movie = {
  __typename?: 'Movie';
  _id: Maybe<Scalars['String']>;
  released: Scalars['Int'];
  tagline: Maybe<Scalars['String']>;
  title: Scalars['String'];
  persons_acted_in: Maybe<Array<Maybe<Person>>>;
  persons_directed: Maybe<Array<Maybe<Person>>>;
  persons_produced: Maybe<Array<Maybe<Person>>>;
  persons_wrote: Maybe<Array<Maybe<Person>>>;
  persons_reviewed: Maybe<Array<Maybe<Person>>>;
};


export type MoviePersons_Acted_InArgs = {
  first: Maybe<Scalars['Int']>;
  offset: Maybe<Scalars['Int']>;
  orderBy: Maybe<Array<Maybe<_PersonOrdering>>>;
  filter: Maybe<_PersonFilter>;
};


export type MoviePersons_DirectedArgs = {
  first: Maybe<Scalars['Int']>;
  offset: Maybe<Scalars['Int']>;
  orderBy: Maybe<Array<Maybe<_PersonOrdering>>>;
  filter: Maybe<_PersonFilter>;
};


export type MoviePersons_ProducedArgs = {
  first: Maybe<Scalars['Int']>;
  offset: Maybe<Scalars['Int']>;
  orderBy: Maybe<Array<Maybe<_PersonOrdering>>>;
  filter: Maybe<_PersonFilter>;
};


export type MoviePersons_WroteArgs = {
  first: Maybe<Scalars['Int']>;
  offset: Maybe<Scalars['Int']>;
  orderBy: Maybe<Array<Maybe<_PersonOrdering>>>;
  filter: Maybe<_PersonFilter>;
};


export type MoviePersons_ReviewedArgs = {
  first: Maybe<Scalars['Int']>;
  offset: Maybe<Scalars['Int']>;
  orderBy: Maybe<Array<Maybe<_PersonOrdering>>>;
  filter: Maybe<_PersonFilter>;
};

export type Acted_In = {
  __typename?: 'ACTED_IN';
  from: Person;
  to: Movie;
  roles: Array<Maybe<Scalars['String']>>;
};

export type Reviewed = {
  __typename?: 'REVIEWED';
  from: Person;
  to: Movie;
  rating: Scalars['Int'];
  summary: Scalars['String'];
};

export type _Neo4jTime = {
  __typename?: '_Neo4jTime';
  hour: Maybe<Scalars['Int']>;
  minute: Maybe<Scalars['Int']>;
  second: Maybe<Scalars['Int']>;
  millisecond: Maybe<Scalars['Int']>;
  microsecond: Maybe<Scalars['Int']>;
  nanosecond: Maybe<Scalars['Int']>;
  timezone: Maybe<Scalars['String']>;
  formatted: Maybe<Scalars['String']>;
};

export type _Neo4jTimeInput = {
  hour: Maybe<Scalars['Int']>;
  minute: Maybe<Scalars['Int']>;
  second: Maybe<Scalars['Int']>;
  millisecond: Maybe<Scalars['Int']>;
  microsecond: Maybe<Scalars['Int']>;
  nanosecond: Maybe<Scalars['Int']>;
  timezone: Maybe<Scalars['String']>;
  formatted: Maybe<Scalars['String']>;
};

export type _Neo4jDate = {
  __typename?: '_Neo4jDate';
  year: Maybe<Scalars['Int']>;
  month: Maybe<Scalars['Int']>;
  day: Maybe<Scalars['Int']>;
  formatted: Maybe<Scalars['String']>;
};

export type _Neo4jDateInput = {
  year: Maybe<Scalars['Int']>;
  month: Maybe<Scalars['Int']>;
  day: Maybe<Scalars['Int']>;
  formatted: Maybe<Scalars['String']>;
};

export type _Neo4jDateTime = {
  __typename?: '_Neo4jDateTime';
  year: Maybe<Scalars['Int']>;
  month: Maybe<Scalars['Int']>;
  day: Maybe<Scalars['Int']>;
  hour: Maybe<Scalars['Int']>;
  minute: Maybe<Scalars['Int']>;
  second: Maybe<Scalars['Int']>;
  millisecond: Maybe<Scalars['Int']>;
  microsecond: Maybe<Scalars['Int']>;
  nanosecond: Maybe<Scalars['Int']>;
  timezone: Maybe<Scalars['String']>;
  formatted: Maybe<Scalars['String']>;
};

export type _Neo4jDateTimeInput = {
  year: Maybe<Scalars['Int']>;
  month: Maybe<Scalars['Int']>;
  day: Maybe<Scalars['Int']>;
  hour: Maybe<Scalars['Int']>;
  minute: Maybe<Scalars['Int']>;
  second: Maybe<Scalars['Int']>;
  millisecond: Maybe<Scalars['Int']>;
  microsecond: Maybe<Scalars['Int']>;
  nanosecond: Maybe<Scalars['Int']>;
  timezone: Maybe<Scalars['String']>;
  formatted: Maybe<Scalars['String']>;
};

export type _Neo4jLocalTime = {
  __typename?: '_Neo4jLocalTime';
  hour: Maybe<Scalars['Int']>;
  minute: Maybe<Scalars['Int']>;
  second: Maybe<Scalars['Int']>;
  millisecond: Maybe<Scalars['Int']>;
  microsecond: Maybe<Scalars['Int']>;
  nanosecond: Maybe<Scalars['Int']>;
  formatted: Maybe<Scalars['String']>;
};

export type _Neo4jLocalTimeInput = {
  hour: Maybe<Scalars['Int']>;
  minute: Maybe<Scalars['Int']>;
  second: Maybe<Scalars['Int']>;
  millisecond: Maybe<Scalars['Int']>;
  microsecond: Maybe<Scalars['Int']>;
  nanosecond: Maybe<Scalars['Int']>;
  formatted: Maybe<Scalars['String']>;
};

export type _Neo4jLocalDateTime = {
  __typename?: '_Neo4jLocalDateTime';
  year: Maybe<Scalars['Int']>;
  month: Maybe<Scalars['Int']>;
  day: Maybe<Scalars['Int']>;
  hour: Maybe<Scalars['Int']>;
  minute: Maybe<Scalars['Int']>;
  second: Maybe<Scalars['Int']>;
  millisecond: Maybe<Scalars['Int']>;
  microsecond: Maybe<Scalars['Int']>;
  nanosecond: Maybe<Scalars['Int']>;
  formatted: Maybe<Scalars['String']>;
};

export type _Neo4jLocalDateTimeInput = {
  year: Maybe<Scalars['Int']>;
  month: Maybe<Scalars['Int']>;
  day: Maybe<Scalars['Int']>;
  hour: Maybe<Scalars['Int']>;
  minute: Maybe<Scalars['Int']>;
  second: Maybe<Scalars['Int']>;
  millisecond: Maybe<Scalars['Int']>;
  microsecond: Maybe<Scalars['Int']>;
  nanosecond: Maybe<Scalars['Int']>;
  formatted: Maybe<Scalars['String']>;
};

export type _Neo4jPointDistanceFilter = {
  point: _Neo4jPointInput;
  distance: Scalars['Float'];
};

export type _Neo4jPoint = {
  __typename?: '_Neo4jPoint';
  x: Maybe<Scalars['Float']>;
  y: Maybe<Scalars['Float']>;
  z: Maybe<Scalars['Float']>;
  longitude: Maybe<Scalars['Float']>;
  latitude: Maybe<Scalars['Float']>;
  height: Maybe<Scalars['Float']>;
  crs: Maybe<Scalars['String']>;
  srid: Maybe<Scalars['Int']>;
};

export type _Neo4jPointInput = {
  x: Maybe<Scalars['Float']>;
  y: Maybe<Scalars['Float']>;
  z: Maybe<Scalars['Float']>;
  longitude: Maybe<Scalars['Float']>;
  latitude: Maybe<Scalars['Float']>;
  height: Maybe<Scalars['Float']>;
  crs: Maybe<Scalars['String']>;
  srid: Maybe<Scalars['Int']>;
};

export enum _RelationDirections {
  In = 'IN',
  Out = 'OUT'
}

export type Query = {
  __typename?: 'Query';
  Person: Maybe<Array<Maybe<Person>>>;
  Movie: Maybe<Array<Maybe<Movie>>>;
};


export type QueryPersonArgs = {
  born: Maybe<Scalars['Int']>;
  name: Maybe<Scalars['String']>;
  _id: Maybe<Scalars['String']>;
  first: Maybe<Scalars['Int']>;
  offset: Maybe<Scalars['Int']>;
  orderBy: Maybe<Array<Maybe<_PersonOrdering>>>;
  filter: Maybe<_PersonFilter>;
};


export type QueryMovieArgs = {
  released: Maybe<Scalars['Int']>;
  tagline: Maybe<Scalars['String']>;
  title: Maybe<Scalars['String']>;
  _id: Maybe<Scalars['String']>;
  first: Maybe<Scalars['Int']>;
  offset: Maybe<Scalars['Int']>;
  orderBy: Maybe<Array<Maybe<_MovieOrdering>>>;
  filter: Maybe<_MovieFilter>;
};

export type Mutation = {
  __typename?: 'Mutation';
  AddPersonActed_in: Maybe<_AddPersonActed_InPayload>;
  RemovePersonActed_in: Maybe<_RemovePersonActed_InPayload>;
  MergePersonActed_in: Maybe<_MergePersonActed_InPayload>;
  AddPersonACTED_IN_rel: Maybe<_AddPersonActed_In_RelPayload>;
  RemovePersonACTED_IN_rel: Maybe<_RemovePersonActed_In_RelPayload>;
  UpdatePersonACTED_IN_rel: Maybe<_UpdatePersonActed_In_RelPayload>;
  MergePersonACTED_IN_rel: Maybe<_MergePersonActed_In_RelPayload>;
  AddPersonDirected: Maybe<_AddPersonDirectedPayload>;
  RemovePersonDirected: Maybe<_RemovePersonDirectedPayload>;
  MergePersonDirected: Maybe<_MergePersonDirectedPayload>;
  AddPersonProduced: Maybe<_AddPersonProducedPayload>;
  RemovePersonProduced: Maybe<_RemovePersonProducedPayload>;
  MergePersonProduced: Maybe<_MergePersonProducedPayload>;
  AddPersonWrote: Maybe<_AddPersonWrotePayload>;
  RemovePersonWrote: Maybe<_RemovePersonWrotePayload>;
  MergePersonWrote: Maybe<_MergePersonWrotePayload>;
  AddPersonFollows: Maybe<_AddPersonFollowsPayload>;
  RemovePersonFollows: Maybe<_RemovePersonFollowsPayload>;
  MergePersonFollows: Maybe<_MergePersonFollowsPayload>;
  AddPersonReviewed: Maybe<_AddPersonReviewedPayload>;
  RemovePersonReviewed: Maybe<_RemovePersonReviewedPayload>;
  MergePersonReviewed: Maybe<_MergePersonReviewedPayload>;
  AddPersonREVIEWED_rel: Maybe<_AddPersonReviewed_RelPayload>;
  RemovePersonREVIEWED_rel: Maybe<_RemovePersonReviewed_RelPayload>;
  UpdatePersonREVIEWED_rel: Maybe<_UpdatePersonReviewed_RelPayload>;
  MergePersonREVIEWED_rel: Maybe<_MergePersonReviewed_RelPayload>;
  CreatePerson: Maybe<Person>;
  UpdatePerson: Maybe<Person>;
  DeletePerson: Maybe<Person>;
  MergePerson: Maybe<Person>;
  AddMoviePersons_acted_in: Maybe<_AddMoviePersons_Acted_InPayload>;
  RemoveMoviePersons_acted_in: Maybe<_RemoveMoviePersons_Acted_InPayload>;
  MergeMoviePersons_acted_in: Maybe<_MergeMoviePersons_Acted_InPayload>;
  AddMoviePersons_directed: Maybe<_AddMoviePersons_DirectedPayload>;
  RemoveMoviePersons_directed: Maybe<_RemoveMoviePersons_DirectedPayload>;
  MergeMoviePersons_directed: Maybe<_MergeMoviePersons_DirectedPayload>;
  AddMoviePersons_produced: Maybe<_AddMoviePersons_ProducedPayload>;
  RemoveMoviePersons_produced: Maybe<_RemoveMoviePersons_ProducedPayload>;
  MergeMoviePersons_produced: Maybe<_MergeMoviePersons_ProducedPayload>;
  AddMoviePersons_wrote: Maybe<_AddMoviePersons_WrotePayload>;
  RemoveMoviePersons_wrote: Maybe<_RemoveMoviePersons_WrotePayload>;
  MergeMoviePersons_wrote: Maybe<_MergeMoviePersons_WrotePayload>;
  AddMoviePersons_reviewed: Maybe<_AddMoviePersons_ReviewedPayload>;
  RemoveMoviePersons_reviewed: Maybe<_RemoveMoviePersons_ReviewedPayload>;
  MergeMoviePersons_reviewed: Maybe<_MergeMoviePersons_ReviewedPayload>;
  CreateMovie: Maybe<Movie>;
  UpdateMovie: Maybe<Movie>;
  DeleteMovie: Maybe<Movie>;
  MergeMovie: Maybe<Movie>;
};


export type MutationAddPersonActed_InArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationRemovePersonActed_InArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationMergePersonActed_InArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationAddPersonActed_In_RelArgs = {
  from: _PersonInput;
  to: _MovieInput;
  data: _Acted_InInput;
};


export type MutationRemovePersonActed_In_RelArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationUpdatePersonActed_In_RelArgs = {
  from: _PersonInput;
  to: _MovieInput;
  data: _Acted_InInput;
};


export type MutationMergePersonActed_In_RelArgs = {
  from: _PersonInput;
  to: _MovieInput;
  data: _Acted_InInput;
};


export type MutationAddPersonDirectedArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationRemovePersonDirectedArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationMergePersonDirectedArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationAddPersonProducedArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationRemovePersonProducedArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationMergePersonProducedArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationAddPersonWroteArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationRemovePersonWroteArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationMergePersonWroteArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationAddPersonFollowsArgs = {
  from: _PersonInput;
  to: _PersonInput;
};


export type MutationRemovePersonFollowsArgs = {
  from: _PersonInput;
  to: _PersonInput;
};


export type MutationMergePersonFollowsArgs = {
  from: _PersonInput;
  to: _PersonInput;
};


export type MutationAddPersonReviewedArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationRemovePersonReviewedArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationMergePersonReviewedArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationAddPersonReviewed_RelArgs = {
  from: _PersonInput;
  to: _MovieInput;
  data: _ReviewedInput;
};


export type MutationRemovePersonReviewed_RelArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationUpdatePersonReviewed_RelArgs = {
  from: _PersonInput;
  to: _MovieInput;
  data: _ReviewedInput;
};


export type MutationMergePersonReviewed_RelArgs = {
  from: _PersonInput;
  to: _MovieInput;
  data: _ReviewedInput;
};


export type MutationCreatePersonArgs = {
  born: Maybe<Scalars['Int']>;
  name: Scalars['String'];
};


export type MutationUpdatePersonArgs = {
  born: Maybe<Scalars['Int']>;
  name: Scalars['String'];
};


export type MutationDeletePersonArgs = {
  name: Scalars['String'];
};


export type MutationMergePersonArgs = {
  born: Maybe<Scalars['Int']>;
  name: Scalars['String'];
};


export type MutationAddMoviePersons_Acted_InArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationRemoveMoviePersons_Acted_InArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationMergeMoviePersons_Acted_InArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationAddMoviePersons_DirectedArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationRemoveMoviePersons_DirectedArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationMergeMoviePersons_DirectedArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationAddMoviePersons_ProducedArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationRemoveMoviePersons_ProducedArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationMergeMoviePersons_ProducedArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationAddMoviePersons_WroteArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationRemoveMoviePersons_WroteArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationMergeMoviePersons_WroteArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationAddMoviePersons_ReviewedArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationRemoveMoviePersons_ReviewedArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationMergeMoviePersons_ReviewedArgs = {
  from: _PersonInput;
  to: _MovieInput;
};


export type MutationCreateMovieArgs = {
  released: Scalars['Int'];
  tagline: Maybe<Scalars['String']>;
  title: Scalars['String'];
};


export type MutationUpdateMovieArgs = {
  released: Maybe<Scalars['Int']>;
  tagline: Maybe<Scalars['String']>;
  title: Scalars['String'];
};


export type MutationDeleteMovieArgs = {
  title: Scalars['String'];
};


export type MutationMergeMovieArgs = {
  released: Maybe<Scalars['Int']>;
  tagline: Maybe<Scalars['String']>;
  title: Scalars['String'];
};

export type MovieFragment = (
  { __typename?: 'Movie' }
  & Pick<Movie, '_id' | 'title' | 'tagline' | 'released'>
);

export type PersonFragment = (
  { __typename?: 'Person' }
  & Pick<Person, '_id' | 'name' | 'born'>
);

export type GetActorsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetActorsQuery = (
  { __typename?: 'Query' }
  & { actors: Maybe<Array<Maybe<(
    { __typename?: 'Person' }
    & { acted_in: Maybe<Array<Maybe<(
      { __typename?: 'Movie' }
      & MovieFragment
    )>>> }
    & PersonFragment
  )>>> }
);

export const MovieFragmentDoc = gql`
    fragment Movie on Movie {
  _id
  title
  tagline
  released
}
    `;
export const PersonFragmentDoc = gql`
    fragment Person on Person {
  _id
  name
  born
}
    `;
export const GetActorsDocument = gql`
    query GetActors {
  actors: Person {
    ...Person
    acted_in {
      ...Movie
    }
  }
}
    ${PersonFragmentDoc}
${MovieFragmentDoc}`;

/**
 * __useGetActorsQuery__
 *
 * To run a query within a React component, call `useGetActorsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetActorsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetActorsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetActorsQuery(baseOptions?: Apollo.QueryHookOptions<GetActorsQuery, GetActorsQueryVariables>) {
        return Apollo.useQuery<GetActorsQuery, GetActorsQueryVariables>(GetActorsDocument, baseOptions);
      }
export function useGetActorsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetActorsQuery, GetActorsQueryVariables>) {
          return Apollo.useLazyQuery<GetActorsQuery, GetActorsQueryVariables>(GetActorsDocument, baseOptions);
        }
export type GetActorsQueryHookResult = ReturnType<typeof useGetActorsQuery>;
export type GetActorsLazyQueryHookResult = ReturnType<typeof useGetActorsLazyQuery>;
export type GetActorsQueryResult = Apollo.QueryResult<GetActorsQuery, GetActorsQueryVariables>;