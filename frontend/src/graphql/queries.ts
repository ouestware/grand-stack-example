import gql from "graphql-tag";
import { fragments } from "./fragments";

export const getActors = gql`
  query GetActors {
    actors: Person {
      ...Person
      acted_in {
        ...Movie
      }
    }
  }
  ${fragments.person}
  ${fragments.movie}
`;
