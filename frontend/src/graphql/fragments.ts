import gql from "graphql-tag";
import { DocumentNode } from "graphql";

export const fragments: { [name: string]: DocumentNode } = {
  movie: gql`
    fragment Movie on Movie {
      _id
      title
      tagline
      released
    }
  `,
  person: gql`
    fragment Person on Person {
      _id
      name
      born
    }
  `,
};
