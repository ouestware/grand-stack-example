import React from "react";
import { PersonFragment, MovieFragment } from "./graphql/types";
import { MovieBox } from "./MovieBox";

interface Props {
  actor: (PersonFragment & { acted_in: Array<MovieFragment | null> | null }) | null;
}

export const ActorBox: React.FC<Props> = (props: Props) => {
  const { actor } = props;

  if (actor === null) return null;
  return (
    <div className="actor">
      <h2>
        {actor.name} - ({actor.born})
      </h2>
      <div className="actor-movies">
        {actor.acted_in?.map((movie) => {
          return <MovieBox key={movie?._id} movie={movie} />;
        })}
      </div>
    </div>
  );
};
