import React from "react";
import { ActorsList } from "./ActorsList";

export const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
        <a className="App-link" href="https://reactjs.org" target="_blank" rel="noopener noreferrer">
          GRAND Stack - Example
        </a>
      </header>
      <main>
        <ActorsList />
      </main>
      <footer></footer>
    </div>
  );
};
