import React from "react";
import { MovieFragment } from "./graphql/types";

interface Props {
  movie: MovieFragment | null;
}

export const MovieBox: React.FC<Props> = (props: Props) => {
  const { movie } = props;
  if (movie === null) return null;
  return (
    <div>
      <h4>
        {movie.title} - ({movie.released})
      </h4>
      <p>{movie.tagline}</p>
    </div>
  );
};
