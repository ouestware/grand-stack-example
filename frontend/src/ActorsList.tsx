import React from "react";
import { useGetActorsQuery } from "./graphql/types";
import { ActorBox } from "./ActorBox";

export const ActorsList: React.FC = () => {
  // Loading the data
  const { data, loading, error } = useGetActorsQuery({ variables: {} });
  return (
    <>
      <h1>Actors</h1>
      {loading && <p>Loading ...</p>}

      {error &&
        error.graphQLErrors.map((e) => {
          return <p>e.message</p>;
        })}

      {data?.actors &&
        data.actors.map((actor) => {
          return <ActorBox actor={actor} />;
        })}
    </>
  );
};
