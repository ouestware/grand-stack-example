export interface config_neo4j {
  url: string;
  login: string;
  password: string;
  options?: { [key: string]: string | boolean | number };
}

export interface Config {
  port: number;
  neo4j: config_neo4j;
}

export const config: Config = {
  port: 4000,
  neo4j: {
    url: "neo4j://localhost:7687",
    login: "neo4j",
    password: "admin"
  }
};
