import { GraphQLResolveInfo, GraphQLObjectType } from "graphql";
import gql from "graphql-tag";

// Pure copy/paste of `npm run generate:schema`
export const typeDefs = gql`
  type Person {
    _id: Long!
    born: Int
    name: String! @index
    acted_in: [Movie] @relation(name: "ACTED_IN", direction: OUT)
    ACTED_IN_rel: [ACTED_IN]
    directed: [Movie] @relation(name: "DIRECTED", direction: OUT)
    produced: [Movie] @relation(name: "PRODUCED", direction: OUT)
    wrote: [Movie] @relation(name: "WROTE", direction: OUT)
    follows: [Person] @relation(name: "FOLLOWS", direction: OUT)
    reviewed: [Movie] @relation(name: "REVIEWED", direction: OUT)
    REVIEWED_rel: [REVIEWED]
  }

  type Movie {
    _id: Long!
    released: Int!
    tagline: String
    title: String! @index
    persons_acted_in: [Person] @relation(name: "ACTED_IN", direction: IN)
    persons_directed: [Person] @relation(name: "DIRECTED", direction: IN)
    persons_produced: [Person] @relation(name: "PRODUCED", direction: IN)
    persons_wrote: [Person] @relation(name: "WROTE", direction: IN)
    persons_reviewed: [Person] @relation(name: "REVIEWED", direction: IN)
  }

  type ACTED_IN @relation(name: "ACTED_IN") {
    from: Person!
    to: Movie!
    roles: [String]!
  }

  type REVIEWED @relation(name: "REVIEWED") {
    from: Person!
    to: Movie!
    rating: Int!
    summary: String!
  }
`;

// Custom resolvers if needed
export const resolvers = {
  // GraphQL query
  Query: {},
  // GraphQL mutation
  Mutation: {}
};

// neo4j-graphql-js config for schema generation
export const config = {
  query: {
    // Exclude labels for query generation
    exclude: []
  },
  mutation: {
    // Exclude labels for mutation generation
    exclude: []
  }
};
