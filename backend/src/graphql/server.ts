import { Express } from "express";
import { Server } from "http";
import { ApolloServer } from "apollo-server-express";
import { makeAugmentedSchema, assertSchema } from "neo4j-graphql-js";
import neo4j from "neo4j-driver";
import { config } from "../config";
import { resolvers, typeDefs, config as gqlConfig } from "./schema";

export function register(server: Server, app: Express): void {
  // create the neo4j driver
  const driver = neo4j.driver(
    config.neo4j.url,
    neo4j.auth.basic(config.neo4j.login, config.neo4j.password)
  );

  // create the Neo4j graphql schema
  const schema = makeAugmentedSchema({
    typeDefs,
    resolvers,
    config: gqlConfig
  });

  // create the graphql server with apollo
  const serverGraphql = new ApolloServer({
    schema,
    context: { driver }
  });

  // Register the graphql server to express
  serverGraphql.applyMiddleware({ app });

  // Sync the Neo4j schema (ie. indexes, constraints)
  assertSchema({ schema, driver, debug: true });
}
