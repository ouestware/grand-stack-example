import express, { Express } from "express";
import http, { Server } from "http";
import { config } from "./config";
import { register as graphql } from "./graphql/server";

// Create an expressjs app
const app: Express = express();

// Create a server
const server: Server = http.createServer(app);

// Create GraphQL
const createSubcriptionServer = graphql(server, app);

// Start the server
server.listen({ port: config.port }, () => {
  console.log(`Server ready at http://localhost:${config.port}`);
});
